-- object.e
/*

=FPOE: object library

* Version: 4.0.5.0
* Author: C A Newbould
* Date: 2020.06.17
* Status: incomplete
* Changes: created
** built-in functions added
** ##len## added

== Library Interface
=== Required library modules
*/
public include io.e

/*
=== Publicly defined functions

<eucode>global function compare :: {o} -> {o} -> i</eucode>

<eucode>global function equal :: {o} -> {o} -> b</eucode>

<eucode>global function find :: o -> {o} -> i -> i</eucode>
*/

function identity(object this) -- :: o -> o
    return this
end function

/*
<eucode>public function len(object this) -- :: o -> i</eucode>
*/

public function len(object this) -- :: o -> i
    if atom(this) then return 1
    else
        if equal(this, EMPTY) then return 0
        else return len(this[1..$-1]) + 1
        end if
    end if
end function

/*
<eucode>global function length :: o -> i</eucode>

<eucode>public function null(object this) -- :: o -> b</eucode>
*/

public function null(object this) -- :: o -> b
    return (len(this) = 0)
end function

/*
<eucode>public function repetition(object this, integer times = 1) -- :: o -> i -> [o]</eucode>
*/

public function repetition(object this, integer times = 1) -- :: o -> i -> [o]
    return map("identity", run(1, times))
end function
