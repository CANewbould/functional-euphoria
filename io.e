-- io.e
/*

=FPOE: IO library to support functional programming

* Version: 4.0.5.4
* Author: C A Newbould
* Date: 2021.12.09
* Status: incomplete
* Changes:
** ##writef## re-defined as a function, so can be used in list-comprehensions
** ##writefln## re-defined as a function, so can be used in list-comprehensions
** ##write## re-defined as a function, so can be used in list-comprehensions
** ##writeln## re-defined as a function, so can be used in list-comprehensions
** ##writeLoop## re-defined as a function, so can be used in list-comprehensions
** VOID defined locally


== Library Interface

=== Required modules

<eucode>public include list.e -- for string</eucode>
*/

public include list.e -- for string
    -- include boolean.e -- for boolean, FALSE, iif, TRUE
    -- include atom.e

/*
=== Constants
<eucode>public constant EOF = -1</eucode>
*/
public constant EOF = -1
constant EOL = '\n'
enum KEYBOARD = 0, SCREEN
enum M_SEEK = 19, M_WHERE
constant UNSET = -1
constant VOID = 0

type filehandle(integer this) -- :: i -> f
    return this = UNSET or this > 2
end type

type fileposition(integer this) --:: i -> p
    return this >= 0 or this = EOF
end type

/*
=== Publicly defined routines
<eucode>public function flength(filehandle fh) -- :: f -> i</eucode>
<eucode>public function flines(filehandle fh) -- :: f -> [[c]]</eucode>
<eucode>public function fread(filehandle fh) -- :: f -> [c]</eucode>
<eucode>public function getPromptedChar(string prompt) -- :: [c] -> c</eucode>
<eucode>public function write(object this = "") -- :: o -> IO</eucode>
<eucode>public function writef(object this, string format) -- :: o -> [c] -> IO</eucode>
<eucode>public function writefln(object this, string format) -- :: o -> [c] -> IO</eucode>
<eucode>public function writeln(object this = "") -- :: o -> IO</eucode>
<eucode>public function writeLoop(strings this) -- :: [[c]] -> IO</eucode>
*/

public function flength(filehandle fh) -- :: f -> i
    if fseek(fh, 0) -- successful move to start of file
    then return iif(fseek(fh, EOF), fwhere(fh), 0) -- len | 0
    else return 0
    end if
end function

public function flines(filehandle fh) -- :: f -> [[c]]
    if fseek(fh, 0) -- successfully back to start of file
    then return apply("getString", fh, EOF)
    else return EMPTY
    end if
end function

public function fread(filehandle fh) -- :: f -> [c]
    if fseek(fh, 0) -- successfully back to start of file
    then return apply("getCh", fh, EOF)
    else return EMPTY
    end if
end function

function getCh(filehandle this) -- :: f -> c
    return getc(this)
end function

public function getPromptedChar(string prompt) -- :: [c] -> c
    write(prompt & ": ")
    sequence s = gets(KEYBOARD)
    return s[1] -- needed to stop the trailing EOL being recycled
end function

function getString(filehandle this) -- :: f -> [c]
    object o = gets(this)
    if atom(o) then return o
    else return o[1..$-1]
    end if
end function

function put(string this) -- :: [c] -> b
    writeln(this)
    return TRUE
end function

function fseek(filehandle fh, fileposition fp) -- :: f -> i -> b
    return not machine_func(M_SEEK, {fh, fp})
end function

function fwhere(filehandle fh) -- :: f -> i
    return machine_func(M_WHERE, fh)
end function

public function write(object this = "") -- :: o -> IO
    if atom(this) then
        if char(this) then puts(SCREEN, this)
        else return VOID
        end if
    else -- sequence
        if string(this) then puts(SCREEN, this)
        else ?this
        end if
    end if
    return VOID
end function

public function writef(object this, string format) -- :: o -> [c] -> IO
    printf(SCREEN, format, {this})
    return VOID
end function

public function writefln(object this, string format) -- :: o -> [c] -> IO
    printf(SCREEN, format & EOL, {this})
    return VOID
end function

public function writeln(object this = "") -- :: o -> IO
    write(this)
    puts(SCREEN, EOL)
    return VOID
end function

public function writeLoop(strings this) -- :: [[c]] -> IO
    map("put", this)
    return VOID
end function

--* Version: 4.0.5.3
--* Author: C A Newbould
--* Date: 2020.06.26
--* Status: incomplete
--* Changes:
--** ##fread## defined
--** //EOF// defined
--** **filehandle** defined
--** ##flength## defined

--* Version: 4.0.5.2
--* Author: C A Newbould
--* Date: 2020.06.21
--* Status: incomplete
--* Changes:
--** ##writeLoop## modified

--* Version: 4.0.5.1
--* Author: C A Newbould
--* Date: 2020.06.18
--* Status: incomplete
--* Changes:
--** ##writeLoop## defined
--** ##write## extended
--** ##writeln## re-defined

--* Version: 4.0.5.0
--* Author: C A Newbould
--* Date: 2020.06.13
--* Status: incomplete
--* Changes: created
