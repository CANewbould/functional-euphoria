-- primes.ex

-- Version 4.0.5.1
-- Author: C A Newbould
-- Date: 2021.12.26
-- Status: operational
-- Changes:
--* revised to use a 'fold'
--* using 'init' and 'last' to make neater

include object.e

function prms(integer x, ints y) -- :: i -> [i] -> [i]
    return iif(not find(FALSE, remainder(x, run(2,x-1))), y&x, y)
end function

-- Heading
writeln("This app calculates all prime numbers up to a user-defined limit")
writeln("----------------------------------------------------------------")

-- Get limit
write("\nMaximum value? ") constant limit = s2i(init(gets(0)))

-- Execution
constant primes = fold("prms", EMPTY, run(2,limit))

-- Display
writef(limit, "Prime numbers up to %d: ")
    mapX("writef", "%d,", init(primes)) writef(last(primes), "%d\n")