--------------------------------------------------------------------------------
--	Library: iup.e
--------------------------------------------------------------------------------
-- Notes:
--
--
--------------------------------------------------------------------------------
--/*
--= Library: <functional-euphoria>iup.e
-- Description: a library to interface the IUP GUI with FPOE.
------
--[[[Version: 4.0.5.0
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2022.01.01
--Status: complete; operational
--Changes:]]]
--* created
--* ##Close## defined
--* ##Open## defined
--* ##Version## defined
--* ##MainLoop## defined
--
--==FPOE library: iup
--
-- This library hold all the functionality for accessing, opening, closing
-- and running IUP in FPOE.
--
-- Utilise this support by adding the following statement to the calling
-- module:
-- <eucode>include iup.e</eucode>
--*/
--------------------------------------------------------------------------------
--/*
--==Interface
--*/
--------------------------------------------------------------------------------
--
--=== Includes
--
--------------------------------------------------------------------------------
include clib.e -- for 'Clib'
include crid.e -- for 'crid', 'Crid', 'fC', 'vfC'
--------------------------------------------------------------------------------
--/*
--=== IUP core
--*/
--------------------------------------------------------------------------------
constant LIB = "libiup.so"
export constant IUP = Clib(LIB)
    export function Close()
        crid CLOSE = Crid("+IupClose", IUP)
        return vfC(CLOSE)
        end function
    export function Open()
        crid OPEN = Crid("+IupOpen", IUP, {C_I, C_P}, C_I)
        return fC(OPEN, {NULL, NULL})
        end function
    export function MainLoop()
        crid ML = Crid("+IupMainLoop", IUP)
        return vfC(ML)
        end function
    export function Version()
        crid VERNO = Crid("+IupVersionNumber", IUP,,C_I)
        return fC(VERNO)/100000
        end function
--------------------------------------------------------------------------------
-- Previous versions
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
