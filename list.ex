-- list.ex

include object.e

writeln("List type testing")

procedure testlist(sequence this) -- :: {o} -> IO
    write("sequence is: ")
    ?this
    writeln(iif(list(this), "    which is a list", "    which is NOT a list!"))
end procedure

testlist({1,2,3})
testlist(EMPTY)
testlist("I am Fred, I am!")
testlist({{}})

function factorial(integer n) -- :: i -> i
    switch n do
        case 0 then return 1
        case else return n * factorial(n-1)
    end switch
end function

write("factorial [1..4] -> ") ?map("factorial", run(1,4))

function ge12(integer this) -- :: i -> b
    return this >= 12
end function

ints two2twenty = 2* run(1,10)
mask GE12 = map("ge12", two2twenty)

puts(1, "[2*x | <- x = [1..10], 2*x >= 12] -> ") ?filterM(GE12, two2twenty)

function sum(list this) -- :: [a] -> a
    return fold("add", 0, this)
end function

writefln(sum(run(1,7)), "sum [1..7] = %d")
writefln(sum(two2twenty), "sum [2,4..20] = %d")

function product(list this) -- :: [a] -> a
    return fold("multiply", 1, this)
end function

writefln(product(run(1,4)), "product [1..4] = %d")

write("Reversing [2,4..20] -> ")
?reverse(two2twenty)

function fibonacci(integer this) -- :: i -> i
    switch this do
    case 0 then return 0
    case 1, 2 then return this - 1
    case else return add(fibonacci(this - 1), fibonacci(this - 2))
    end switch
end function

writefln(fibonacci(8), "fibonacci 8 = %d")
write("map fibonacci [3, 10] -> ") ?map("fibonacci", run(3, 10))

function notin131519(integer this) -- :: i -> b
    return not find(this, {13,15,19})
end function

constant ten2twenty = run(10,20)
puts(1, "[x | x <-[10..20], x /= 13, x /= 15, x /= 19] -> ")
?filterM(map("notin131519", ten2twenty), ten2twenty)

function Gt3(integer this) --:: i -> b
    return this > 3
end function
write("filter (>3) [1,5,3,2,1,6,4,3,2,1] -> ")
?filter("Gt3", {1,5,3,2,1,6,4,3,2,1})

write("filterX(\"gt\", 3, {1,5,3,2,1,6,4,3,2,1}) -> ") ?filterX("gt", 3, {1,5,3,2,1,6,4,3,2,1})

write("filter even [1..10] -> ")
function even(integer this) -- :: i -> b
    return not remainder(this,2)
end function
?filterM(map("even", run(1,10)), run(1,10))

string fred = "My name is Fred Smith"
writefln(fred, "fred = '%s'")
writefln(toUpper(fred), "toUpper(fred) -> %s")

writefln(toLower(fred), "toLower(fred) -> %s")

writeln("*** Testing alternative functional definitions of base functions ***")

writefln(len(3), "len(3) = %d")
writefln(len(EMPTY), "len(EMPTY) -> %d")
writefln(len({2,6,5,8,1}), "len({2,6,5,8,1}) -> %d")

function quicksort(list this) -- :: [a] -> [a]
    if length(this) < 2 then return this
    else return quicksort(filterM(mapX("lt", this[1], this[2..$]), this[2..$]))
         & this[1]
         & quicksort(filterM(mapX("ge", this[1], this[2..$]), this[2..$]))
    end if
    return this[1]
end function
write("quicksort [2,5,1,34,3,7,2,1,55,8]) -> ") ?quicksort({2,5,1,34,3,7,2,1,55,8})

writeln("Trying extending 'maximum' definition to a list")

function maxf(list this) -- :: [a] -> a
    return fold("maximum", 0, this)
end function

writefln(maxf({2,5,1,34,3,7,2,1,55,8}), "Maximum = %d, using 'fold' through 'maxf'")

constant UNDEFINED = 999
function minr(list this) -- :: [a] -> a
    switch length(this) do
        case 0 then return warn(UNDEFINED)
        case 1 then return this[$]
        case else return minimum(this[$], minr(this[1..$-1]))
    end switch
end function

writefln(minr({2,5,1,34,3,7,2,1,55,8}), "Minimum = %d, using recursion")
function maxr(list this) -- :: [a] -> a
    switch length(this) do
        case 0 then return warn(UNDEFINED)
        case 1 then return this[1]
        case else return maximum(this[1], maxr(this[2..$]))
    end switch
end function

writefln(maxr({2,5,1,34,3,7,2,1,55,8}), "Maximum = %d, using recursion, through 'maxr'")

writeln("Testing end condition")
writefln(maxr(EMPTY), "maxr(EMPTY) = %d")

writeln("Testing 'replicate' using recursion")

function replicate(object this, integer times) -- :: o -> i -> [o]
    list ret = EMPTY
    if times <= 0 then
        ret = EMPTY
    else
        return cons(this, replicate(this, times-1))
    end if
    return ret
end function

write("replicate(1, -3) -> ") ? replicate(1, -3)
write("replicate(23, 5) -> ") ? replicate(23, 5)

writeln("*** Testing string splitting ***")
string sentence = "I am the way the truth"

function numberOf(atom val, list this) -- :: a -> [a] -> i
    return sum(mapX("eq", val, this))
end function

writef(numberOf(' ', sentence), "There are %d occurrences of ' ' in the sentence: ")
writeln(sentence)

write("Instances of \" \" in the sentence -> ") ?matchAll(" ", sentence)

writeln("Testing separate(sentence, matchAll(\" \", sentence))")
writeLoop(separate(sentence, matchAll(" ", sentence)))

writeln("Testing split(sentence, \" \")")
writeLoop(split(sentence, " "))

function speller(string name) -- :: [c] -> [c]
    return head(name) & " is for " & name
end function

writeln("Testing the 'speller' function from the Haskell Course")


strings alpha = {"apple", "banana", "carrot", "deer", "elephant"}
writeLoop(map("speller", alpha))
