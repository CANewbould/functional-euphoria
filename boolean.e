-- boolean.e

/*
=A basic boolean library

*Version: 4.0.5.2
*Author: C A Newbould
*Date: 2021.12.10
*Status: operational; complete
*Changes:
** modified ##iif## to give it defaults

==Details

This library contains the basic elements of a boolean (TRUE/FALSE) library for
the Euphoria Programming Language.

This documentation describes the relevant definitions, which are cast in a manner
consistent with functional programming.

Note that many Euphoria expressions do not strictly return //TRUE//, but instead
return a positive value.

Also note that the 'if .. then .. else .. end if' construct is generally laid out
as follows:
<eucode>if ...
then ..
[[elsif .. then ..]..]
else ..
end if</eucode>
*/

/*
===Constants
*/

public constant FALSE = 0 -- v4.0.5.1
public constant TRUE = not FALSE -- v4.0.5.1

/*
=== Publicly defined types
*/

public type boolean(integer this) --> (TRUE or FALSE)
    return TRUE -- all integers apply -- v4.0.5.1
end type

/*
=== Publicly defined function
*/

public function iif(boolean this, object ifTrue = "TRUE", object ifFalse = "FALSE") -- :: b -> o -> o -> o
    if this then return ifTrue
    else return ifFalse
    end if
end function

/*
Parameters:
# //this//: a boolean expression
# //ifTrue//: the return value if the expression produces a //TRUE// value
# //ifFalse//: the return value if the expression produces a //FALSE// value

Returns:

an **object**: one or other possible value

*/

--*Version: 4.0.5.1
--*Author: C A Newbould
--*Date: 2021.10.04
--*Status: operational; complete
--*Changes:
--** modified //FALSE// and //TRUE// to Euphoria standard

--*Version: 4.0.5.0
--*Author: C A Newbould
--*Date: 2020.06.12
--*Status: operational; complete
