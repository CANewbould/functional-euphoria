-- io.ex

-- Testbed for IO routines

include io.e

constant ioh = open("io.ex", "r")
writefln(flength(ioh), "'io.ex' is %d characters long")

writeln("The contents are:")
writefln(fread(ioh), "**START**\n%s\n**END**")

constant words = {"I", "am", "the", "way"}
writeln("Using 'writeLoop' to output the words")
writeLoop(words)

writeln("Testing 'flines'")
writeln("**START**")

writeLoop(flines(ioh))
writeln("**END**")
