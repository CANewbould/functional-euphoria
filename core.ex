-- core.ex

include object.e

write("?o2s(4) -> ") ?o2s(4)

--?o2s('d')
writefln(cons('f', o2s('d')), "cons('f', o2s('d')) -> %s") 

write("cons(3) -> ") ?cons(3)

write("cons(1, cons(2,cons(3))) -> ") ?cons(1, cons(2,cons(3)))

write("run(2,0) -> ") ?run(2,0)
write("run(8,8) -> ") ?run(8,8)
write("run(4,12) -> ") ?run(4,12)

write("range(3,43,7) -> ") ?range(3,43, 7)

write("[1,4..20] -> ") ? cons(1, 4*run(1,5))

write("cons(EMPTY,EMPTY) -> ") ?cons(EMPTY,EMPTY)
write("cons(cons(1,EMPTY), cons(EMPTY,EMPTY)) -> ") ?cons(cons(1,EMPTY), cons(EMPTY,EMPTY))
write("cons(EMPTY, cons(EMPTY,EMPTY)) -> ") ?cons(EMPTY, cons(EMPTY,EMPTY))
