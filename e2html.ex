-- extract.ex

-- Version 4.0.5.1
-- Author: C A Newbould
-- Date: 2021.12.26
-- Status: operational
-- Changes:
--* Revised to allow extracted line to take any byte not just char

include object.e

write("File name: ")
constant fname = init(gets(0)), fh = open(fname, "r"), source = flines(fh)

boolean creole = FALSE
function extract(ints line) --> [c] -> b
    integer l = len(line)
    if l >= 2 and equal(line[1..2], "/*") then creole = TRUE return FALSE
    elsif l >= 2 and equal(line[1..2], "*/") then creole = FALSE return FALSE
    else -- go on to look at line
    end if
    if creole then return TRUE
    else -- other line
    end if
    return FALSE -- catch doc-start & doc-end
end function

close(fh)

constant out = filterS("extract", source), cname = fname & ".cr", crfh = open(cname, "w")
puts(crfh,foldS("concat",EMPTY,out)) 
close(crfh)
constant hname = fname & ".html"
if system_exec("creole " & cname, 0) then writeln(hname & " couldn't be created")
else writeln(hname & " created")
end if