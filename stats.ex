-- stats.ex

/*
=Test/demo file for the FPOE Statistics library

* Version: 4.0.5.1
* Author: C A Newbould
* Date: 2021.12.08
* Status: incomplete
* Changes:
** test of ##correlation## added
** ##covariance## re-calculated for sample

*/

include stats.e

list s_and_p = {1692,1978,1884,2151,2519}
list abc = {68,102,110,112,154}

writef(mean(abc), "The arithmetic mean of the ABC values is %g, ")
writef(sd(abc), "with standard deviation of %.2f\n")
writef(mean(s_and_p,GEOM), "The geometric mean of the S&P values is %g\n")

writef(covariance(s_and_p,abc), "The covariance is %g\n")
writef(correlation(s_and_p, abc), "Hence the correlation coefficient is %.3f\n")

integer l = len(abc)
writef(covariance(s_and_p, abc)*l / (l-1), "Covariance estimate is %g\n")

/*
* Version: 4.0.5.0
* Author: C A Newbould
* Date: 2021.12.08
* Status: incomplete
* Changes:
** created
*/