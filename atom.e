-- atom.e

/*
=FPOE: atom library

* Version: 4.0.5.2
* Author: C A Newbould
* Date: 2020.06.26
* Status: incomplete
* Changes: created
** ##run## moved to //core.e//
** //EMPTY// moved to //core.e//

This library holds the functions which operate on atom-based expressions.

This library also contains a set of masking functions, which act as "normal" functional
representations of infix operators. For example:

<eucode>4 + 13</eucode>

can be represented as:

<eucode>add(4, 13)</eucode>

These masking functions are used in list comprehensions, in generating
accumulator functions and in maps (using function ##mapx##) which take a
comparator argument.

== Library Interface
=== Required library modules
*/
public include core.e

constant ARITH = {'+', '-', '*', '/', '=', '%', '(', ')', '[', ']', '^', '{', '}', '>', '<'}
constant ESCAPES = {'\n', '\t', '\r'}
constant LETTERS = run('a', 'z')
                 & run('A', 'Z')
constant NUMBERS = run('0', '9')
constant OTHERS = {163, '$', '&', '_', '\\', '?', '@', '#', '~', ':', ';', ' '} -- £ (not working)
constant QUOTES = {'\'', '"', '!', ',', '.', '`', ' '}
constant CHARS = NUMBERS & LETTERS & ESCAPES & QUOTES & ARITH & OTHERS

/*
=== Publicly defined types
<eucode>global type atom(object this) --> a single-valued object</eucode>
<eucode>public type char(integer this) --> a character value</eucode>
<eucode>global type integer(atom this) --> an integer-valued single-valued object</eucode>
*/
public type char(integer this) -- :: i -> c
    switch find(this, CHARS) do
    case 0 then return FALSE -- no side-effects
    case else return TRUE
    end switch
end type

type threeway(integer this) -- :: i -> i
    return find(this, {-1, 0, 1})
end type

/*
=== Publicly defined functions
*/

/*
<eucode>public function abs(atom this) -- :: a -> a</eucode>
<eucode>public function add(atom this, atom that) -- :: a -> a -> a</eucode>
<eucode>public function eq(atom this, atom that) -- :: a -> a -> b</eucode>
<eucode>public function ge(atom this, atom that) -- :: a -> a -> b</eucode>
<eucode>public function gt(atom this, atom that) -- :: a -> a -> b</eucode>
<eucode>public function le(atom this, atom that) -- :: a -> a -> b</eucode>
<eucode>public function lower(char this) -- :: c -> c</eucode>
<eucode>public function lt(atom this, atom that) -- :: a -> a -> b</eucode>
<eucode>public function maximum(atom this, atom that) -- :: a -> a -> a</eucode>
<eucode>public function minimum(atom this, atom that) -- :: a -> a -> a</eucode>
<eucode>public function mod(integer this, integer that) -- :: i -> i -> i</eucode>
<eucode>public function multiply(atom this, atom that) -- :: a -> a -> a</eucode>
<eucode>public function ne(atom this, atom that) -- :: a -> a -> b</eucode>
<eucode>public function sign(atom this) -- :: a -> i</eucode>
<eucode>public function square(atom this) -- :: a -> a</eucode>
<eucode>public function subtract(atom this, atom that) -- :: a -> a -> a</eucode>
<eucode>public function upper(char this) -- :: c -> c</eucode>
*/

public function abs(atom this) -- :: a -> a
    return iif(this > 0, this, (0-this))
end function

public function add(atom this, atom that) -- :: a -> a -> a
    return this + that
end function

public function eq(atom this, atom that) -- :: a -> a -> b
    return this = that
end function

public function ge(atom this, atom that) -- :: a -> a -> b
    return this >= that
end function

public function gt(atom this, atom that) -- :: a -> a -> b
    return this > that
end function

public function le(atom this, atom that) -- :: a -> a -> b
    return this <= that
end function

public function lower(char this) -- :: c -> c
    if find(this, run('A', 'Z')) then return add(this, 32)
    else return this
    end if
end function

public function lt(atom this, atom that) -- :: a -> a -> b
    return this < that
end function

public function maximum(atom this, atom that) -- :: a -> a -> a
    return iif(this >= that, this, that)
end function

public function minimum(atom this, atom that) -- :: a -> a -> a
    return iif(this <= that, this, that)
end function

public function mod(integer this, integer that) -- :: i -> i -> i
    return remainder(this, that)
end function

public function multiply(atom this, atom that) -- :: a -> a -> a
    return this * that
end function

public function ne(atom this, atom that) -- :: a -> a -> b
    return this != that
end function

public function sign(atom this) -- :: a -> i
    if this > 0 then return 1
    elsif this < 0 then return -1
    else return 0
    end if
end function

public function square(atom this) -- :: a -> a
    return multiply(this, this)
end function

function subtract(atom this, atom that) -- :: a -> a -> a
    return add(this, 0-that)
end function

public function upper(char this) -- :: c -> c
    if find(this, run('a', 'z')) then return subtract(this, 32)
    else return this
    end if
end function

--* Version: 4.0.5.1
--* Author: C A Newbould
--* Date: 2020.06.19
--* Status: incomplete
--* Changes: created
--** ##run## moved - to define **char**
--** //EMPTY// moved
--** ##sign## defined

--* Version: 4.0.5.0
--* Author: C A Newbould
--* Date: 2020.06.17
--* Status: incomplete
--* Changes: created
--** ##abs## moved
--** ##add## moved
--** ##eq## moved
--** ##ge## moved
--** ##gt## moved
--** ##lt## moved
--** ##le## moved
--** ##max## moved
--** ##min## moved
--** ##mod## moved
--** ##multiply## moved
--** ##ne## moved
--** ##square## moved
--** ##power## moved
--** **char** defined
