-- core.e

/*

= FPOE: core library
* Version: 4.0.5.2
* Author: C A Newbould
* Date: 2021.12.06
* Status: incomplete
* Changes:
** ##range## defined
** ##run## consequently modified
** ##cons## default added

==The FPOE core library
This library contains a few basic tools needed for functional programming
in Open Euphoria. It sits at the bottom of the Functional Programming in Open
Euphoria (FPOE) hierarchy and doesn't need to be called directly in any user
module.

Note that the function ##run## is placed here (and not in //list.e//, where it
logically belongs) to aid definitions made earlier in the chain of modules.

==Interface
=== Required library modules
The module
<eucode>public include boolean.e</eucode>

is strictly not required but is "passed through" the hierarchy to ensure its
availability elsewhere.
*/
public include boolean.e
/*
=== Publicly defined expressions
<eucode>public constant EMPTY = {}</eucode>
*/
public constant EMPTY = {}
--public constant UNDEFINED = -999
/*
=== Publicly defined functions
<eucode>public function cons(object this, sequence that = EMPTY) -- :: o -> {o} -> {o}</eucode>
Adds an object to the front of a **sequence** (proxy for a List - defined later).
*/

public function cons(object this, sequence that = EMPTY) -- :: o -> {o} -> {o}
    return o2s(this) & that
end function

/*
<eucode>public function o2s(object this) -- :: o -> {o}</eucode>
Simply wraps an object up as a sequence.
*/

public function o2s(object this) -- :: o -> {o}
    return {this}
end function

/*
<eucode>public function range(integer this, integer that, integer step = 1) -- :: i -> i -> i ->[i]</eucode>
Generates a series of integers: {this,..,<that>} in steps.

Note that the terminating value will not necessarily be included in the returned value; only
if ##remainder##(//that//-//this//, //steps//) = 0,
*/

public function range(integer this, integer that, integer step = 1) -- :: i -> i -> i -> [i]
    if that < this then return EMPTY
        else
            switch (that-this) do
                case 0 then return o2s(this)
                case else return cons(this, range(this+step, that, step))
            end switch
        end if
    end function

/*
<eucode>public function run(integer this, integer that) -- :: i -> i -> [i]</eucode>
A simplified version of ##range##.
*/

public function run(integer this, integer that) -- :: i -> i -> [i]
    return range(this, that)
end function

public function warn(integer warnvalue) -- :: i -> i
    printf(1, "** WARNING **: code = %d\n", {warnvalue})
    return warnvalue
end function

/*
<eucode>public function warn(integer warnvalue) -- :: i -> i</eucode>
A service routine to handle warnings.
*/

--= FPOE: core library
--* Version: 4.0.5.1
--* Author: C A Newbould
--* Date: 2020.06.29
--* Status: incomplete
--* Changes: created
--** ##warn## defined
--** //UNDEFINED// defined

--* Version: 4.0.5.0
--* Author: C A Newbould
--* Date: 2020.06.26
--* Status: incomplete
--* Changes: created
--** ##run## moved from //atom.e//
--** //EMPTY// moved from //atom.e//
--** ##cons## defined
--** ##o2s## defined
