--stats.e

/*
=Statistics library

* Version: 4.0.5.2
* Author: C A Newbould
* Date: 2021.12.08
* Status: incomplete
* Changes:
** ##correlation## defined

This library holds a set of functions which exploit the list-processing options
provided by the //functional-euphoria// repository.

At present the following are defined:

* correlation(list, list) :: [a] -> [a] -> a
* covariance(list, list) :: [a] -> [a] -> a
* mean(list,[__ARITH__|GEOM|HARM]) :: [a] -> i -> a
* product(list) :: [a] -> a
* sd(list) :: [a] -> a
* sum(list) :: [a] -> a
* variance(list) :: [a] -> a

*/

public include object.e -- general access to all FPOE libraries

export function correlation(list this, list that) --> :: [a] -> [a] -> a
    return covariance(this, that) / (sd(this) * sd(that))
end function

export function covariance(list this, list that) --> [a] -> [a] -> a
    return sum(this*that) / len(this) - mean(this) * mean(that)
end function

export enum ARITH, GEOM, HARM
export function mean(list this, integer typ = ARITH) --> :: [a] -> i -> a
    integer l = len(this)
    switch typ do
        case GEOM then return power(product(this), 1 / l)
        case HARM then return l / sum(1/this)
        case else
            return sum(this) / l
    end switch
end function

export function product(list this) --> [a] -> a
    return fold("multiply", 1, this)
end function

export function sd(list this) --> [a] -> a
    return sqrt(variance(this))
end function

export function sum(list this) --> [a] -> a
    return fold("add", 0, this)
end function

export function variance(list this) --> [a] -> a
    return covariance(this, this)
end function

/*
* Version: 4.0.5.1
* Author: C A Newbould
* Date: 2021.12.08
* Status: incomplete
* Changes:
** everything changed to //export//
** ##sum## defined
** ##product## defined
** previous functions revised to refer to these
** simplified to disregard sample-estimation
** ##sd## defined

* Version: 4.0.5.0
* Author: C A Newbould
* Date: 2021.12.07
* Status: incomplete
* Changes:
** created
** ##mean## defined
** ##covariance## defined
** hence ##variance## defined
*/