# README #
### What does this repository offer? ###

* A set of standalone libraries suitable for functional programming in Open Euphoria
* Complementary examples, as demonstrations of using the libraries
* An extension library, and examples, to showcase use of the libraries for statistical purposes
* An Operating System independence

### Requirements ###

* Requires Open Euphoria Version 4.0.5 or later

### How do I get set up? ###

* You need first to have installed [Open Euphoria](https://openeuphoria.org/wiki/view/DownloadEuphoria.wc)
* Download the library modules from here
* Place the files in a suitable location on your machine - possibly separated into libraries and demos
* Create a *eu.cfg* to accompany the demos if you opt for separation; it should reflect the relative position of these locations
* Either double-click on a demo file (if so set up) or open a terminal and call via **eui**

### Feedback and contributions ###

* Feedback is always welcome
* The author of these libraries would be grateful to anyone writing further functions, demos or tests
* Further routines will be added as and when thought fit

### Who do I contact? ###

* For these libraries: Bitbucket User: CANewbould
* For Open Euphoria: [OE's main page](https://openeuphoria.org/index.wc)