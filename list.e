-- list.e
/*

= FPOE: list library

* Version: 4.0.5.4
* Author: C A Newbould
* Date: 2021.12.26
* Status: incomplete
* Changes:
** ##s2i## defined
** ##head## defined
** ##init## defined
** ##last## defined
** ##tail## defined

==The FPOE list library
This library offers definitions of list types (specialised forms of the
fundamental **sequence** type) which are needed for functional programming
in Open Euphoria.

As well as the **list** type (based on **atom** elements) it is possible to
have lists of lists, as exemplified by the **strings** type: a **list** of
(sub)**string**s.

The library also includes some essential utility functions for manipulating
lists:

* maps: which apply a function to all the elements in a list
* folds: which apply an accumulator function to all the elements of a list
* filters: where only selected elements of a list are copied to an output list

(Filtering can be delivered either through a matched **list** of **boolean**
values, a **mask**,
or via a filtering function which, akin to mapping,
is applied to each element of a list, in turn.)

Other basic list-related utilities are also included in the library.

Some functions generate new lists, rather than operating on existing ones.
One such function, ##apply## is used for reading the contents of file streams.

Others operate by sub-dividing lists. Examples are ##separate## and ##split##;
the latter specifically applying to **string**s.

NB: At present the library holds parallel definitions of several functions:
one for a straight **list**, another where the function requires a parameter
before operating on a list; another which operates strictly on a **sequence**,
but which will only be guaranteed to work on a List.

==Interface
=== Required library modules
<eucode>public include atom.e</eucode>
*/

public include atom.e

/*
=== Publicly defined List types

Lists are OE sequences where all elements are of the same type.

They are signified in the shorthand coding system by square brackets, to distinguish
from the standard curly brackets for the **sequence** type.

A "simple" list, where all elements are atoms, is deemed to be of type **list**. The
more generalised form of List, which doesn't have a specific OE type designation, is
a **list** of a **list**. Examples are matrices and **strings** - a **list** of
**string**s.

Other short-form codes are largely explanatory, being the first letter of the
designated OE type: thus //b// for **boolean**, //c// for **char**, //a// for
**atom**, etc. Double-colons signify the start of defining the functional process.

*/

function _integer_(object this) --> o :: i
    return integer(this)
end function

/*
<eucode>public type ints(sequence this) --> :: {o} -> [i]</eucode>
*/

public type ints(sequence this) --> :: {o} -> [i]
    return not find(FALSE, map("_integer_", this))
end type

function _atom_(object this) --> o :: a
    return atom(this)
end function

/*
<eucode>public type list(sequence this) --> :: {o} -> [a]</eucode>
*/

public type list(sequence this) --> :: {o} -> [a]
    return not find(FALSE, map("_atom_", this))
end type

/*
<eucode>public type mask(sequence this) --> :: {o} -> [b]</eucode>
*/

public type mask(sequence this) --> :: {o} -> [b]
    return not find(FALSE, map("boolean", this))
end type

/*
<eucode>public type string(sequence this) --> :: {o} -> [c]</eucode>
*/

public type string(sequence this) --> :: {o} -> [c]
    for n = 1 to length(this) do
        if char(this[n])
        then continue
        else return FALSE
        end if
    end for
    return TRUE
end type

/*
<eucode>public type strings(sequence this) --> :: {o} -> [[c]]</eucode>
*/

public type strings(sequence this) --> :: {o} -> [[c]]
    return not find(FALSE, map("string", this))
end type

/*

*/
/*
=== Publicly defined List operations

In the short-form "syntax" shown after each function's signature,
ordinary brackets are use to indicate the process of the designated function.


==== Filters:

Filters act on the different kinds of Lists. They produce sub-lists with elements
selected according to the filter. Filters can be made up of masks or by use of
functions which operate on an **object** and return a **boolean**.
    
The first form applies a mask to a list:

<eucode>public function filterM(mask that, list this) --> :: [b] -> [a] -> [a]</eucode>
*/
public function filterM(mask that, list this) --> :: [b] -> [a] -> [a]
    if length(that) >= length(this)
    then
        list ret = EMPTY
        for n = length(this) to 1 by -1 do
            if that[n]
            then ret = cons(this[n], ret)
            else --ret = ret
            end if
        end for
        return ret
    else return this
    end if
end function
/*
The other forms apply a function to each element of the list.

This first requires a fully-specified function; the second allows the function to
operate on a value, presented as an argument. The third version applies to all Lists,
including lists of lists.

<eucode>public function filter(string fn, list this) --> :: (a -> b) -> [a] -> [a]</eucode>
*/
public function filter(string fn, list this, integer func = routine_id(fn)) --> :: (a -> b) -> [a] -> [a]
    switch length(this) do
        case 0 then return EMPTY
        case 1 then return iif(call_func(func, {this[1]}), o2s(this[1]), EMPTY)
        case else
            sequence before = filter(fn, this[1..$-1], func)
            boolean last = call_func(func, {this[$]})
            return iif(last, append(before, this[$]), before)
    end switch
end function

/*
<eucode>public function filterX(string fn, atom target, list this) --> :: (a -> b) -> a -> [a] -> [a]</eucode>
*/

public function filterX(string fn, atom target, list this, integer func = routine_id(fn)) --> :: (a -> b) ->a -> [a] -> [a]
    switch length(this) do
        case 0 then return EMPTY
        case 1 then return iif(call_func(func, {this[1], target}), o2s(this[1]), EMPTY)
        case else
            sequence before = filterX(fn, target, this[1..$-1], func)
            boolean last = call_func(func, {this[$], target})
            return iif(last, append(before, this[$]), before)
    end switch
end function

/*
<eucode>public function filterS(string fn, sequence this) --> :: (a -> b) -> [o] -> [o]</eucode>
*/
public function filterS(string fn, sequence this, integer func = routine_id(fn)) --> :: (o -> b) -> [o] -> [o]
    switch length(this) do
        case 0 then return EMPTY
        case 1 then return iif(call_func(func, {this[1]}), o2s(this[1]), EMPTY)
        case else
            sequence before = filterS(fn, this[1..$-1], func)
            boolean last = call_func(func, {this[$]})
            return iif(last, append(before, this[$]), before)
    end switch
end function
/*

Examples:
<eucode>filterX("gt",6,run(1,10))</eucode>
which yields: 

//{7,8,9,10}//

<eucode>strings source = ....
function extract(string line) --> [c] -> b
    ... 
strings out = filterS("extract", source)</eucode>
*/

/*
==== Folds:

Folds apply a function to a list in such a way as to accummulate a single value.

Folds need two things:
# a starting (init)ial value
# a sense of direction: left-to-Right (default) or right-to-Left

The direction of "travel" signifies the designation of the fold, so, for example, going
left-to-right is a Right Fold.

At present the library contains just two Right folds: one for lists and the more general
form for Lists:
<eucode>public function fold(string fn, object init, list this) --> :: (a -> o -> o) -> [a] -> o</eucode>
*/

public function fold(string fn, object init, list this, integer func = routine_id(fn)) --> :: {a -> o -> o) -> [a] -> o
    switch length(this) do
        case 0 then return EMPTY
        case 1 then return call_func(func, {this[1], init})
        case else return call_func(func, {this[$], fold(fn, init, this[1..$-1], func)})
    end switch
end function

/*
<eucode>public function foldS(string fn, object init, sequence this) --> :: (o -> o -> o) -> [o] -> o</eucode>

a typical use is on **strings**, for example:

<eucode>foldS("concat",EMPTY,<strings-list>) --> ([c] -> [c] -> [c]) -> [[c]] -> [c]</eucode>
*/

public function foldS(string fn, object init, sequence this, integer func = routine_id(fn)) --> :: {o -> o -> o) -> [o] -> o
    switch length(this) do
        case 0 then return EMPTY
        case 1 then return call_func(func, {this[1], init})
        case else return call_func(func, {this[$], foldS(fn, init, this[1..$-1], func)})
    end switch
end function

/*
==== Maps:

Maps apply a function to a list, transforming each and every element according to the same
rule.

Three forms are offered:

<eucode>public function map(string fn, list this) --> :: (a -> a) -> {a} -> {a}</eucode>
*/

public function map(string fn, sequence this, integer func = routine_id(fn)) --> :: (o -> o) -> {o} -> {o}
    switch length(this) do
        case 0 then return EMPTY
        case 1 then return o2s(call_func(func, o2s(this[1])))
        case else
            for n = 1 to length(this) do
                this[n] = call_func(func, o2s(this[n]))
            end for
            return this
    end switch
end function

/*
<eucode>public function mapX(string fn, object val, sequence this) --> :: ((o -> o) -> o) -> {o} -> {o}</eucode>
*/

public function mapX(string fn, object val, sequence this, integer func = routine_id(fn)) --> :: ((o -> o) -> o) -> {o} -> {o}
    switch length(this) do
        case 0 then return EMPTY
        case 1 then return {call_func(func, {this[1], val})}
        case else return append(mapX(fn, val, this[1..$-1], func), call_func(func, {this[$], val}))
    end switch
end function

/*
<eucode>public function mapS(string fn, sequence this) --> :: (o -> o) -> {o} -> {o}</eucode>

The first applies to a single-valued List (**list**, **ints** or **string**);
as does the second, but with the added option for the mapping function to take a parameter.

The third applies to all Lists, including lists-of-lists, for example **strings**.
*/

public function mapS(string fn, sequence this, integer func = routine_id(fn)) --> :: (o -> o) -> {o} -> {o}
    switch length(this) do
        case 0 then return EMPTY
        case 1 then return o2s(call_func(func, o2s(this[1])))
        case else return append(mapS(fn, this[1..$-1], func), call_func(func, o2s(this[$])))
    end switch
end function

/*
=== Derived functions
These are functions, defined in a declarative mode, which operate on lists and are
defined using list-comprehensions.
<eucode>public function reverse(list this) --> :: [a] -> [a]</eucode>
*/

public function reverse(list this) --> :: [a] -> [a]
    switch length(this) do
        case 0, 1 then return this
        case else return cons(this[$], reverse(this[1..$-1]))
    end switch
end function

/*
<eucode>public function toLower(string this) --> :: [c] -> [c]</eucode>
*/

public function toLower(string this) --> :: [c] -> [c]
    return map("lower", this)
end function

/*
<eucode>public function toUpper(string this) -->  :: [c] -> [c]</eucode>
*/

public function toUpper(string this) -->  :: [c] -> [c]
    return map("upper", this)
end function

/*
==== Other useful functions
These are functions which are considered generally useful for functional programming
using Open Euphoria.

(For those interested in such niceties, functional programming is necessarily rigid in its
directional requirements as well as being very conscious of types.
In contrast, Open Euphoria tries to make its routines as general
as it can - **object**-focussed, without any obvious policy with regard to direction.
For example, functions like ##append## and ##prepend## are Left-oriented,
whereas ##find## and ##match## are Right-oriented.
This distinction often means that the two contexts require synonym functions whose
effects are identical but whose order of processing/argument-defining is reversed.
This distinction is slightly blurred because some FP functions, eg. ##add##, //appear//
to be directionless, whereas most make explicit their directionality.)

<eucode>public function in(list this, atom that) --> :: [a] -> a -> b</eucode>
This is a FP synonym for ##find##, especially suitable for list processing.
*/

public function in(list this, atom that) --> :: [a] -> a -> b
    return find(that, this)
end function

/*
<eucode>public function concat(string this, string that) -- :: [c] -> [c] -> [c]</eucode>
This function is useful for mapping **strings** into a **string**. It is analogous to ##add##
for **list**s and is a synonym for ##append## as applied to **string**s (##cons## is the
true synonym for ##append##).
*/

public function concat(string this, string that) -- :: [c] -> [c] -> [c]
    return that & this
end function

public function apply(string fn, object args, atom ending, integer func = routine_id(fn)) --> :: (o -> o) -> a -> {o}
    sequence ret = EMPTY
    if atom(args) then args = o2s(args)
    else args = args
    end if
    object next = call_func(func, args)
    while goOn(next, ending) do
        ret = append(ret, next)
        next = call_func(func, args)
    end while
    return ret
end function

function divide(list this, integer that) --> :: [a] -> i -> [[a]]
    if that > 0 then
        if that < length(this) then return {this[1..that-1], this[that+1..$]}
        else return this
        end if
    else return this
    end if
end function

function goOn(object this, object that) --> :: o -> o -> b
    if atom(this)
    then if atom(that)
         then return not (this = that)
         else return not (equal(o2s(this), that))
         end if
    else if atom(that) -- this is a sequence
         then return not (equal(this, o2s(that)))
         else return not equal(this, that)
         end if
    end if
end function

/*
<eucode>public function head(list l) --> :: [i] -> i</eucode>
*/

public function head(list l) --> :: [i] -> i
    return l[1]
    end function

/*
<eucode>public function init(list l) --> :: [i] -> [i]</eucode>
*/

public function init(list l) --> :: [i] -> [i]
    return l[1..$-1]
    end function

/*
<eucode>public function last(list l) --> :: [i] -> i</eucode>
*/

public function last(list l) --> :: [i] -> i
    return l[$]
    end function

public function matchAll(string part, string whole) --> :: [c] -> [c] -> [i]
    switch match(part, whole) do
    case 0 then return EMPTY
    case 1 then return {1}
    case else
        if match(part, whole[$-length(part)+1..$])
        then return matchAll(part, whole[1..$-1])
                    & length(whole) - length(part) +1
        else return matchAll(part, whole[1..$-1])
        end if
    end switch
end function

/*
<eucode>public function s2i(string n) --> :: [c] -> i</eucode>
This converts a string containing digits into the corresponding integer.
*/

public function s2i(string n) --> :: [c] -> i
    return fold("s2", 0, n)
    end function

function s2(char x, integer y) --> :: c -> i -> i
    return (x-48) + 10*y
    end function


public function separate(list this, list that) --> :: [a] -> [i] -> [[a]]
    switch length(that) do
        case 0 then return this
        case 1 then return divide(this, that[1])
        case else sequence ret = separate(this, that[1..$-1])
                    return ret[1..$-1] & divide(ret[$], (that[$] - that[$-1]))
    end switch
end function

public function split(string this, string splitter) --> :: [c] -> [c] -> [[c]]
    list found = matchAll(splitter, this)
    return separate(this, found)
end function

/*
<eucode>public function tail(list l) --> :: [i] -> [i]</eucode>
*/

public function tail(list l) --> :: [i] -> [i]
    return l[2..$]
    end function


--* Version: 4.0.5.3
--* Author: C A Newbould
--* Date: 2021.12.12
--* Status: incomplete
--* Changes:
--** extended documentation

--* Version: 4.0.5.2
--* Author: C A Newbould
--* Date: 2021.12.11
--* Status: incomplete
--* Changes:
--** ##concat## defined
--** ##foldS## generalised for all sequence types

--* Version: 4.0.5.1
--* Author: C A Newbould
--* Date: 2021.12.10
--* Status: incomplete
--* Changes:
--** ##filterx## defined
--** ##mapx_## and ##mapx## argument type changed
--** ##filterS## generalised for all sequence types

--* Version: 4.0.5.0
--* Author: C A Newbould
--* Date: 2020.06.17
--* Status: incomplete
--* Changes: created
